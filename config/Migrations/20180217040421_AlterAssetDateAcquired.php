<?php
use Migrations\AbstractMigration;

class AlterAssetDateAcquired extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('asset')
            ->renameColumn('dateAquired', 'dateAcquired')
            ->update();
    }
}
