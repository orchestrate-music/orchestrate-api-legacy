<?php
use Migrations\AbstractMigration;

class EnforceUniqueMemberEmail extends AbstractMigration
{
    public function up()
    {
        $this->table('member')
            ->changeColumn('email', 'string', [
                'null' => true,
                'limit' => 90,
                'default' => null,
            ])
            ->update();

        $this->execute("UPDATE member SET email = NULL WHERE email = ''");
        $sql = <<<SQL
UPDATE member
SET email = NULL
WHERE id IN (
    SELECT memberId
    FROM (
        SELECT member1.id AS memberId
        FROM member AS member1
        JOIN member AS member2
        WHERE member1.email = member2.email
        AND member1.id > member2.id
    ) AS duplicateMemberEmailIds
)
SQL;
        $this->execute($sql);

        // Unique where not null, mysql doesn't need anything else to enforce this
        // https://stackoverflow.com/questions/3712222
        $this->table('member')
            ->addIndex(['email'], ['unique' => true])
            ->update();
    }

    public function down()
    {
        $this->table('member')
            ->removeIndex(['email'])
            ->update();

        $this->execute("UPDATE lacborga_manager.`member` SET email = '' WHERE email is NULL");

        $this->table('member')
            ->changeColumn('email', 'string', [
                'null' => false,
                'default' => null,
            ])
            ->update();
    }
}
