ALTER TABLE `band_manager`.`ensemble_membership` 
DROP INDEX `uq_ensemble_membership` ;

ALTER TABLE `band_manager`.`ensemble_membership`
ADD COLUMN `dateJoined` DATETIME NULL AFTER `ensembleId`,
ADD COLUMN `dateLeft` DATETIME NULL AFTER `dateJoined`;

UPDATE
	`band_manager`.`ensemble_membership` membership,
	`band_manager`.`member` member 
SET membership.dateJoined = member.dateJoined,
	membership.dateLeft = member.dateLeft
WHERE member.id = membership.memberId;