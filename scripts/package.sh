#!/bin/bash

# Remove dev packages
php composer.phar update --no-dev

rm -f dist/band-manager-backend.tar.gz

mkdir -p dist/staging
cp -rf config/ src/ vendor/ webroot/ dist/staging/
rm -f dist/staging/config/app.php

cd dist/staging/
tar -czf band-manager-backend.tar.gz * --exclude=band-manager-backend.tar.gz
mv band-manager-backend.tar.gz ../
cd ../..

rm -rf dist/staging/
