<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EnsembleMembershipFixture
 *
 */
class EnsembleMembershipFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ensemble_membership';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'memberId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ensembleId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dateJoined' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'dateLeft' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_ensemble_membership_memberId_idx' => ['type' => 'index', 'columns' => ['memberId'], 'length' => []],
            'fk_ensemble_membership_ensemble_idx' => ['type' => 'index', 'columns' => ['ensembleId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'uq_ensemble_membership' => ['type' => 'unique', 'columns' => ['memberId', 'ensembleId'], 'length' => []],
            'fk_ensemble_membership_ensemble' => ['type' => 'foreign', 'columns' => ['ensembleId'], 'references' => ['ensemble', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'fk_ensemble_membership_memberId' => ['type' => 'foreign', 'columns' => ['memberId'], 'references' => ['member', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'memberId' => 1,
            'ensembleId' => 1,
            'dateJoined' => '2016-11-14 05:43:48',
        ],
        [
            'id' => 2,
            'memberId' => 1,
            'ensembleId' => 2,
            'dateJoined' => '2016-11-14 05:43:48',
        ],
        [
            'id' => 3,
            'memberId' => 2,
            'ensembleId' => 1,
            'dateJoined' => '2016-11-14 05:43:48',
            'dateLeft' => '2016-11-21 05:43:48',
        ],
        [
            'id' => 4,
            'memberId' => 2,
            'ensembleId' => 3,
            'dateJoined' => '2016-11-14 05:43:48',
        ],
    ];
}
