<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MemberInstrumentFixture
 *
 */
class MemberInstrumentFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'member_instrument';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'memberId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'instrumentName' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'fk_instrument_member_idx' => ['type' => 'index', 'columns' => ['memberId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_instrument_member' => ['type' => 'foreign', 'columns' => ['memberId'], 'references' => ['member', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'memberId' => 1,
            'instrumentName' => 'Clarinet'
        ],
        [
            'id' => 2,
            'memberId' => 1,
            'instrumentName' => 'Flute'
        ],
        [
            'id' => 3,
            'memberId' => 2,
            'instrumentName' => 'Trumpet'
        ],
    ];
}
