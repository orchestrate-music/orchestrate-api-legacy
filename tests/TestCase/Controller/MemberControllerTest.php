<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MemberController;
use App\Test\TestCase\Controller\RestTestCase;
use App\Controller\Component\MemberServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\Exception\RecordNotFoundException;
use App\Test\TestCase\DomainObjectFactory;

class MemberControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->MemberService = new MemberServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->MemberService);

        parent::tearDown();
    }

    public function testIndex() {
        $responseData = $this->get('/member');

        $this->assertResponseOk();
        $this->assertResponseContains('member');

        $this->assertCount(3, $responseData->member);
        foreach($responseData->member as $member) {
            $this->assertNotNull($member->ensemble);
        }
    }

    public function testViewExists() {
        $responseData = $this->get('/member/1');

        $this->assertResponseOk();

        $this->assertEquals(1, $responseData->member->id);
        $this->assertCount(2, $responseData->member->ensemble);
    }

    public function testViewNotExists() {
        $this->get('/member/0');

        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $newMember = DomainObjectFactory::createMemberMap();
        unset($newMember['id']);
        $newMember['firstName'] = 'John';

        $responseData = $this->post('/member', $newMember);

        $this->assertResponseOk();
        $this->assertEquals('John', $responseData->member->firstName);
        $this->assertGreaterThan(0, $responseData->member->id);

        // Make sure it was actually created.
        $this->MemberService->findById($responseData->member->id);
    }

    public function testAddInvalid() {
        $newMember = DomainObjectFactory::createMemberMap();
        $newMember['phoneNo'] = '041'; // Should be 12 digits

        $countBefore = $this->MemberService->count();
        $this->post('/member', $newMember);
        $countAfter = $this->MemberService->count();

        $this->assertResponseCode(400);
        $this->assertEquals($countBefore, $countAfter);
    }

    public function testAddWithInstruments() {
        $newMember = DomainObjectFactory::createMemberMap();
        unset($newMember['id']);
        $newMember['instruments'] = ['A', 'B', 'C'];

        $responseData = $this->post('/member', $newMember);

        $this->assertResponseOk();
        $this->assertCount(3, $responseData->member->instruments);

        // Make sure it was actually created.
        $this->MemberService->findById($responseData->member->id);
    }

    public function testEdit() {
        $modifiedMember = $this->MemberService->findById(1);
        $modifiedMember->firstName = 'John';

        $responseData = $this->put('/member/1', $modifiedMember->toArray());

        $this->assertResponseOk();

        $this->assertEquals('John', $responseData->member->firstName);
        $this->assertEquals(1, $responseData->member->id);

        // Make sure it was actually updated.
        $dbMember = $this->MemberService->findById($responseData->member->id);
        $this->assertEquals('John', $dbMember->firstName);
    }

    public function testEditInvalid() {
        $modifiedMember = $this->MemberService->findById(1);
        $modifiedMember->phoneNo = '041'; // Should be 12 chars

        $this->put('/member/1', $modifiedMember->toArray());

        $this->assertResponseCode(400);

        // Make sure it wasn't actually updated.
        $dbMember = $this->MemberService->findById(1);
        $this->assertNotEquals('041', $dbMember->phoneNo);
    }

    public function testEditNotExists() {
        $modifiedMember = $this->MemberService->findById(1);
        $modifiedMember->firstName = 'John';

        $this->put('/member/0', $modifiedMember->toArray());

        $this->assertResponseCode(404);
    }

    public function testDeleteExists() {
        $this->delete('/member/1');

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->MemberService->findById(1);
    }

    public function testDeleteNotExist() {
        $this->delete('/member/0');

        $this->assertResponseCode(404);
    }

    public function testEmbeddedInstruments() {
        $responseData = $this->get('/member/1');

        $this->assertResponseOk();
        $this->assertNotNull($responseData->member->instruments);
        $this->assertCount(2, $responseData->member->instruments);
    }

    public function testCurrentMembers() {
        $responseData = $this->get('/member/current');

        $this->assertResponseOk();
        $this->assertNotNull($responseData->member);
        $this->assertCount(2, $responseData->member);
        foreach($responseData->member as $member) {
            $this->assertNotNull($member->ensemble);
        }
    }
}
