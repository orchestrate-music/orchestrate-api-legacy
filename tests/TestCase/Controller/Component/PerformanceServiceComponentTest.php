<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\PerformanceServiceComponent;
use App\Test\TestCase\DomainObjectFactory;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

class PerformanceServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Concert',
        'app.Ensemble',
        'app.Performance',
        'app.Member',
        'app.PerformanceMember',
        'app.Score',
        'app.PerformanceScore',
    ];

    public $PerformanceService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->PerformanceService = new PerformanceServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->PerformanceService);
        parent::tearDown();
    }

    public function testFindAllPerformances()
    {
        $performances = $this->PerformanceService->findAll();
        $this->assertCount(4, $performances);
    }

    public function testGetPerformancesByConcertId()
    {
        $performance = $this->PerformanceService->findByConcertId(1);
        $this->assertCount(3, $performance);
        $performance = $this->PerformanceService->findByConcertId(2);
        $this->assertCount(1, $performance);
        $performance = $this->PerformanceService->findByConcertId(3);
        $this->assertCount(0, $performance);
    }

    public function testGetPerformance()
    {
        $performance = $this->PerformanceService->findById(1);
        $this->assertEquals(1, $performance->id);
        $this->assertNotNull($performance->concert);
        $this->assertNotNull($performance->ensemble);
    }

    public function testGetPerformanceContainsMembers()
    {
        $performance = $this->PerformanceService->findById(1);
        $this->assertNotNull($performance->member);
    }

    public function testGetPerformanceContainedMembersHasNoPersonalDetails()
    {
        $performance = $this->PerformanceService->findById(1);
        $this->checkMemberListForPersonalData($performance->member);
    }

    private function checkMemberListForPersonalData($memberList)
    {
        if ($memberList) {
            foreach ($memberList as $member) {
                $this->assertNull($member->email);
                $this->assertNull($member->phoneNo);
                $this->assertNull($member->address);
            }
        }
    }

    public function testGetNonexistantPerformance()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $performance = $this->PerformanceService->findById(0);
    }

    public function testAddPerformance()
    {
        $performanceMap = DomainObjectFactory::createPerformanceMap();
        $performanceMap['concertId'] = 3;
        $performanceMap['ensembleId'] = 1;

        $performance = $this->PerformanceService->create($performanceMap);

        $performance = $this->PerformanceService->findByConcertId(3);
        $this->assertCount(1, $performance);
    }

    public function testAddPerformanceContainedMembersHasNoPersonalDetails()
    {
        $performanceMap = DomainObjectFactory::createPerformanceMap();
        $performanceMap['concertId'] = 3;
        $performanceMap['ensembleId'] = 1;
        $performanceMap['members'] = [1, 2];

        $performance = $this->PerformanceService->create($performanceMap);

        $this->checkMemberListForPersonalData($performance->member);
    }

    public function testAddInvalidPerformanceEnsemble()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $performanceMap = DomainObjectFactory::createPerformanceMap();
        $performanceMap['concertId'] = 1;
        $performanceMap['ensembleId'] = 0;

        $performance = $this->PerformanceService->create($performanceMap);
    }

    public function testAddInvalidPerformanceConcert()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $performanceMap = DomainObjectFactory::createPerformanceMap();
        $performanceMap['concertId'] = 0;
        $performanceMap['ensembleId'] = 1;

        $performance = $this->PerformanceService->create($performanceMap);
    }

    public function testAddPerformanceWithEmbeddedMembers()
    {
        $performanceMap = DomainObjectFactory::createPerformanceMap();
        $performanceMap['concertId'] = 3;
        $performanceMap['ensembleId'] = 1;
        $performanceMap['members'] = [1, 2, 3];

        $performance = $this->PerformanceService->create($performanceMap);

        $performance = $this->PerformanceService->findById($performance->id);
        $this->assertCount(3, $performance->member);
    }

    public function testAddPerformanceWithEmbeddedScores()
    {
        $performanceMap = DomainObjectFactory::createPerformanceMap();
        $performanceMap['concertId'] = 3;
        $performanceMap['ensembleId'] = 1;
        $performanceMap['scores'] = [1, 2, 3];

        $performance = $this->PerformanceService->create($performanceMap);

        $performance = $this->PerformanceService->findById($performance->id);
        $this->assertCount(3, $performance->score);
    }

    public function testAddPerformanceWithEmbeddedMembersHasNoPersonalDetails()
    {
        $performanceMap = DomainObjectFactory::createPerformanceMap();
        $performanceMap['concertId'] = 3;
        $performanceMap['ensembleId'] = 1;
        $performanceMap['members'] = [1, 2, 3];

        $performance = $this->PerformanceService->create($performanceMap);

        $this->checkMemberListForPersonalData($performance->member);
    }

    public function testSetPerformanceEnsemble()
    {
        $performance = $this->PerformanceService->findById(1);
        $this->assertNotEquals(3, $performance->ensembleId);

        $performanceMap = [
            'ensembleId' => 3
        ];
        $this->PerformanceService->update(1, $performanceMap);

        $performance = $this->PerformanceService->findById(1);
        $this->assertEquals(3, $performance->ensembleId);
    }

    public function testSetInvalidPerformanceEnsemble()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $performanceMap = [
            'ensembleId' => 0
        ];

        $this->PerformanceService->update(1, $performanceMap);
    }

    public function testSetInvalidPerformanceConcert()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $performanceMap = [
            'concertId' => 0
        ];

        $this->PerformanceService->update(1, $performanceMap);
    }

    public function testSetUnknownPerformanceEnsemble()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceService->update(0, 1);
    }

    public function testUpdatePerformanceWithEmbeddedMembers()
    {
        $performanceMap = [
            'members' => [1, 2, 3]
        ];

        $performance = $this->PerformanceService->update(1, $performanceMap);

        $performance = $this->PerformanceService->findById(1);
        $this->assertCount(3, $performance->member);
    }

    public function testUpdatePerformanceWithEmbeddedMembersHasNoPersonalDetails()
    {
        $performanceMap = [
            'members' => [1, 2, 3]
        ];

        $performance = $this->PerformanceService->update(1, $performanceMap);

        $this->checkMemberListForPersonalData($performance->member);
    }

    public function testDeletePerformance()
    {
        $this->PerformanceService->delete(1);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceService->findById(1);
    }

    public function testDeleteUnknownPerformance()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceService->delete(0);
    }
}
