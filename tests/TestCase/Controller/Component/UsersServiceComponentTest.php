<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\UsersServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\DomainObjectFactory;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\I18n\Time;

class UsersServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Role',
        'app.UserRole',
        'app.Member',
        'app.Ensemble',
        'app.EnsembleMembership',
        'app.EnsembleRole',
    ];

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->UsersService = new UsersServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->UsersService);

        parent::tearDown();
    }

    public function testFindAll()
    {
        $users = $this->UsersService->findAll();

        $this->assertCount(3, $users);
    }

    public function testFindAllHasRolesEmbedded()
    {
        $users = $this->UsersService->findAll();

        foreach ($users as $user) {
            $this->assertNotNull($user->roles);
        }
    }

    public function testFindByIdExists()
    {
        $user = $this->UsersService->findById(1);
        $this->assertEquals(1, $user->id);
    }

    public function testFindByIdHasRolesEmbedded()
    {
        $user = $this->UsersService->findById(1);
        $this->assertCount(2, $user->roles);
        $user = $this->UsersService->findById(2);
        $this->assertCount(1, $user->roles);
    }

    public function testFindByIdNotExists()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $user = $this->UsersService->findById(0);
    }

    public function testCreateValidUser()
    {
        $user = DomainObjectFactory::createUserMap();
        $user['username'] = 'newuser';

        $countBefore = $this->UsersService->count();
        $this->UsersService->create($user);
        $countAfter = $this->UsersService->count();

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    public function testFindByIdForAuthorisationIncludesRoles()
    {
        $user = $this->UsersService->findByIdForAuthorisation(1);

        $this->assertNotNull($user->member);
        $this->assertCount(2, $user->member->ensemble);
        $this->assertCount(1, $user->member->ensemble[0]->roles);
        $this->assertCount(0, $user->member->ensemble[1]->roles);
    }

    public function testFindByIdForAuthorisationFiltersOutLeftEnsembles()
    {
        $user = $this->UsersService->findByIdForAuthorisation(2);

        $this->assertNotNull($user->member);
        $this->assertCount(1, $user->member->ensemble);
        $this->assertCount(1, $user->member->ensemble[0]->roles);
    }

    public function testCreateInvalidUser()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $user = DomainObjectFactory::createUserMap();
        $user['created'] = 'notdate';

        $countBefore = $this->UsersService->count();
        $this->UsersService->create($user);
        $countAfter = $this->UsersService->count();

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testCreateDuplicateUser()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $user = DomainObjectFactory::createUserMap();
        $user['username'] = 'test1';

        $this->UsersService->create($user);
    }

    public function testUpdateValidUser()
    {
        $user = $this->UsersService->findById(1);
        $user->created = new Time('2018-12-12');

        $this->UsersService->update(1, $user->toArray());

        $user = $this->UsersService->findById(1);
        $this->assertEquals(new Time('2018-12-12'), $user->created);
    }

    public function testUpdateInvalidUser()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $user = $this->UsersService->findById(1);
        $user['username'] = 'test2';

        $this->UsersService->update(1, $user->toArray());
    }

    public function testDeleteUser()
    {
        $this->UsersService->delete(1);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->UsersService->findById(1);
    }

    public function testDeleteUnknown()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->UsersService->delete(0);
    }
}
