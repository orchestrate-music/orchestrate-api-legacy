<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\EmailServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\MockEmailProvider;

class EmailServiceComponentTest extends TestCase
{
    public $EmailService;
    private $emailProvider;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->EmailService = new EmailServiceComponent($registry);
        $this->emailProvider = new MockEmailProvider($this->EmailService);
    }

    public function tearDown()
    {
        unset($this->EmailService);

        parent::tearDown();
    }

    public function testSend()
    {
        $emailData = [
            'toEmail' => 'test@test.com',
            'toName' => 'Test Person',
            'subject' => 'Test Subject',
            'content' => 'What a piece of cake ;)',
        ];

        $this->EmailService->send($emailData);

        $this->assertEquals($emailData, $this->emailProvider->getLastSend());
    }
}
