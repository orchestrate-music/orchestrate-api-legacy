<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\EnsembleServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\DomainObjectFactory;
use Cake\Datasource\Exception\RecordNotFoundException;
use App\Test\TestCase\CustomAssert;
use Cake\I18n\Time;

class EnsembleServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Ensemble',
        'app.Member',
        'app.EnsembleMembership',
        'app.Concert',
        'app.Performance',
    ];

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->EnsembleService = new EnsembleServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->EnsembleService);

        parent::tearDown();
    }

    public function testFindAll()
    {
        $ensembles = $this->EnsembleService->findAll();

        $this->assertCount(3, $ensembles);
    }

    public function testFindAllContiansPerformanceCounts()
    {
        $ensembles = $this->EnsembleService->findAll();

        CustomAssert::entityIdHasSpecifiedPerformanceCount($ensembles, 1, 2);
        CustomAssert::entityIdHasSpecifiedPerformanceCount($ensembles, 2, 1);
        CustomAssert::entityIdHasSpecifiedPerformanceCount($ensembles, 3, 1);
    }

    public function testFindAllOnlyContainsActiveMembers()
    {
        $ensembles = $this->EnsembleService->findAll();

        foreach ($ensembles as $ensemble) {
            foreach ($ensemble->member as $member) {
                $dateLeft = $member->_joinData->dateLeft;
                $this->assertTrue($dateLeft == NULL || $dateLeft > new Time());
            }
        }
    }

    public function testFindByIdExists()
    {
        $ensemble = $this->EnsembleService->findById(1);

        $this->assertEquals(1, $ensemble->id);
    }

    public function testFindByIdOnlyContainsActiveMembers()
    {
        $ensemble = $this->EnsembleService->findById(1);

        foreach ($ensemble->member as $member) {
            $dateLeft = $member->_joinData->dateLeft;
            $this->assertTrue($dateLeft == NULL || $dateLeft > new Time());
        }
    }

    public function testFindByIdNotExists()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');

        $ensemble = $this->EnsembleService->findById(0);
    }

    public function testCreateValidEnsemble()
    {
        $ensemble = DomainObjectFactory::createEnsembleMap();

        $countBefore = $this->EnsembleService->count();
        $this->EnsembleService->create($ensemble);
        $countAfter = $this->EnsembleService->count();

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    public function testCreateInvalidEnsemble()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $ensemble = DomainObjectFactory::createEnsembleMap();
        $ensemble['name'] = ''; // Empty is invalid

        $countBefore = $this->EnsembleService->count();
        $this->EnsembleService->create($ensemble);
        $countAfter = $this->EnsembleService->count();

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testUpdateValidEnsemble()
    {
        $ensemble = $this->EnsembleService->findById(1);
        $ensemble->name = 'Leeming Concert Band';

        $this->assertNotEquals('Leeming Concert Band', $this->EnsembleService->findById(1)->name);
        $this->EnsembleService->update(1, $ensemble->toArray());

        $this->assertEquals('Leeming Concert Band', $this->EnsembleService->findById(1)->name);
    }

    public function testUpdateInvalidEnsemble()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $ensemble = $this->EnsembleService->findById(1);
        $ensemble->name = '';

        $this->EnsembleService->update(1, $ensemble->toArray());
    }

    public function testDeleteEnsemble()
    {
        $this->EnsembleService->delete(1);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->EnsembleService->findById(1);
    }

    public function testDeleteUnknown()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->EnsembleService->delete(0);
    }
}
