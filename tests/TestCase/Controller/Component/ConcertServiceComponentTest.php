<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\ConcertServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\DomainObjectFactory;

class ConcertServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Concert',
        'app.Ensemble',
        'app.Performance',
        'app.Member',
        'app.PerformanceMember',
    ];

    private $ConcertService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->ConcertService = new ConcertServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->ConcertService);
        parent::tearDown();
    }

    public function testFindAll()
    {
        $concerts = $this->ConcertService->findAll();
        $this->assertCount(3, $concerts);
    }

    public function testFindByIdExists()
    {
        $concert = $this->ConcertService->findById(1);
        $this->assertEquals(1, $concert->id);
    }

    public function testFindByIdNotExists()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $concert = $this->ConcertService->findById(0);
    }

    public function testCreateValidConcert()
    {
        $concert = DomainObjectFactory::createConcertMap();

        $countBefore = $this->ConcertService->count();
        $this->ConcertService->create($concert);
        $countAfter = $this->ConcertService->count();

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    public function testCreateInvalidConcert()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $concert = DomainObjectFactory::createConcertMap();
        $concert['date'] = 'INVALID DATE';

        $countBefore = $this->ConcertService->count();
        $this->ConcertService->create($concert);
        $countAfter = $this->ConcertService->count();

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testUpdateValidConcert()
    {
        $concert = $this->ConcertService->findById(1);
        $concert->location = 'Scout Hall';

        $this->ConcertService->update(1, $concert->toArray());

        $this->assertEquals('Scout Hall', $this->ConcertService->findById(1)->location);
    }

    public function testUpdateInvalidConcert()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $concert = $this->ConcertService->findById(1);
        $concert->date = 'INVALID TIME';

        $this->ConcertService->update(1, $concert->toArray());
    }

    public function testDeleteConcert()
    {
        $this->ConcertService->delete(1);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->ConcertService->findById(1);
    }

    public function testDeleteUnknown()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->ConcertService->delete(0);
    }

    public function testGetConcertsByMemberId()
    {
        $concerts = $this->ConcertService->findByMemberId(1);
        $concerts = $concerts->toArray();

        $this->assertCount(1, $concerts);
        $this->assertEquals(1, $concerts[0]->id);
        $this->assertCount(1, $concerts[0]->performance);
        $this->assertEquals(1, $concerts[0]->performance[0]->id);
        $this->assertEquals(1, $concerts[0]->performance[0]->ensemble->id);
    }

    public function testGetConcertsByNonExistantMember()
    {
        $concerts = $this->ConcertService->findByMemberId(0);
        $concerts = $concerts->toArray();

        $this->assertCount(0, $concerts);
    }
}
