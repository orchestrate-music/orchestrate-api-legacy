<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Controller\MemberControllerAuth;
use App\Test\TestCase\DomainObjectFactory;

class MemberInstrumentControllerAuthTest extends AuthTestCase {
    public function testIndex() {
        $endpoint = '/member/1/member_instrument';
        $this->assertAdminAndEditorOnlyGetAccessForEndpoint($endpoint, 'admin');
    }

    public function testAdd() {
        $endpoint = '/member/1/member_instrument';
        $newInstrument = [
            'instrumentName' => 'Timpani'
        ];

        $this->assertStandardPostAccessForEndpoint($endpoint, $newInstrument);
    }

    public function testDelete() {
        $endpoint = '/member/1/member_instrument/delete';
        $deletedInstrument = ['instrumentName' => 'Clarinet'];
        $mockMemberInstrument = DomainObjectFactory::createMemberInstrumentMap();
        $recreateCallback = $this->generateNewObjectFunctionWithId('MemberInstrument', $mockMemberInstrument, 1);
        $this->assertStandardPostAccessForEndpoint($endpoint, $deletedInstrument, $recreateCallback);
    }
}
