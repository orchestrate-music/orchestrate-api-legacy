<?php
namespace App\Test\TestCase\Controller;

use App\Controller\AssetController;
use App\Test\TestCase\Controller\RestTestCase;
use App\Controller\Component\AssetServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\Exception\RecordNotFoundException;
use App\Test\TestCase\DomainObjectFactory;

class AssetControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->AssetService = new AssetServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->AssetService);

        parent::tearDown();
    }

    public function testIndex() {
        $responseData = $this->get('/asset');

        $this->assertResponseOk();
        $this->assertResponseContains('asset');
        $this->assertCount(3, $responseData->asset);
    }

    public function testViewExists() {
        $responseData = $this->get('/asset/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->asset->id);
    }

    public function testViewNotExists() {
        $this->get('/asset/0');

        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $newAsset = DomainObjectFactory::createAssetMap();
        unset($newAsset['id']);
        $newAsset['description'] = 'Bass Clarinet';

        $responseData = $this->post('/asset', $newAsset);

        $this->assertResponseOk();
        $this->assertEquals('Bass Clarinet', $responseData->asset->description);
        $this->assertGreaterThan(0, $responseData->asset->id);

        // Make sure it was actually created.
        $this->AssetService->findById($responseData->asset->id);
    }

    public function testAddInvalid() {
        $newAsset = DomainObjectFactory::createAssetMap();
        $newAsset['description'] = '';

        $countBefore = $this->AssetService->count();
        $this->post('/asset', $newAsset);
        $countAfter = $this->AssetService->count();

        $this->assertResponseCode(400);
        $this->assertEquals($countBefore, $countAfter);
    }

    public function testEdit() {
        $modifiedAsset = $this->AssetService->findById(1);
        $modifiedAsset->description = 'Bass Clarinet';

        $responseData = $this->put('/asset/1', $modifiedAsset->toArray());

        $this->assertResponseOk();
        $this->assertEquals('Bass Clarinet', $responseData->asset->description);
        $this->assertEquals(1, $responseData->asset->id);

        // Make sure it was actually updated.
        $dbAsset = $this->AssetService->findById($responseData->asset->id);
        $this->assertEquals('Bass Clarinet', $dbAsset->description);
    }

    public function testEditInvalid() {
        $modifiedAsset = $this->AssetService->findById(1);
        $modifiedAsset->description = '';

        $this->put('/asset/1', $modifiedAsset->toArray());

        $this->assertResponseCode(400);

        // Make sure it wasn't actually updated.
        $dbAsset = $this->AssetService->findById(1);
        $this->assertNotEquals('Bass Clarinet', $dbAsset->description);
    }

    public function testEditNotExists() {
        $modifiedAsset = $this->AssetService->findById(1);
        $modifiedAsset->description = 'Bass Clarinet';

        $this->put('/asset/0', $modifiedAsset->toArray());

        $this->assertResponseCode(404);
    }

    public function testDeleteExists() {
        $this->delete('/asset/1');

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetService->findById(1);
    }

    public function testDeleteNotExist() {
        $this->delete('/asset/0');
        $this->assertResponseCode(404);
    }
}
