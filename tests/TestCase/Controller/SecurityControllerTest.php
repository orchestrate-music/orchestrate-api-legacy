<?php

namespace App\Test\TestCase\Controller;

use App\Controller\SecurityController;
use Cake\TestSuite\IntegrationTestCase;
use App\Controller\Component\UsersServiceComponent;
use Cake\Controller\ComponentRegistry;
use App\Test\TestCase\DomainObjectFactory;

class SecurityControllerTest extends IntegrationTestCase
{
    public $fixtures = [
        'app.Users',
        'app.Role',
        'app.UserRole',
        'app.Asset'
    ];

    private $user;
    private $headers;

    private $validUserCredentials = ['username' => 'testuser', 'password' => 'password'];
    private $invalidUserCredentials = ['username' => 'testuser', 'password' => 'incorrect'];
    private $unknownUserCredentials = ['username' => 'unknown', 'password' => 'incorrect'];

    public function setUp()
    {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->UsersService = new UsersServiceComponent($registry);

        // Create a temporary user to auth tests.
        $userMap = DomainObjectFactory::createUserMap();
        $userMap['username'] = $this->validUserCredentials['username'];
        $userMap['password'] = $this->validUserCredentials['password'];
        $this->user = $this->UsersService->create($userMap);

        $this->headers = ['Accept' => 'application/json'];
    }

    public function tearDown()
    {
        $this->UsersService->delete($this->user->id);

        unset($this->UsersService);

        parent::tearDown();
    }

    public function testLoginValid()
    {
        $response = $this->login($this->validUserCredentials);
        $this->assertResponseOk();
        $this->assertNotNull($response->user->roles);
    }

    private function login($credentials)
    {
        $this->defaultRequestSetup();
        $this->post('/security/login', $credentials);

        $response = $this->getResponseBody();

        if (isset($response->user)) {
            $user = (array) $response->user;
            $this->session([
                'Auth' => ['User' => $user]
            ]);
        }

        return $response;
    }

    private function defaultRequestSetup()
    {
        $this->configRequest([
            'headers' => $this->headers
        ]);
        $this->enableCsrfToken();
    }

    private function getResponseBody()
    {
        return json_decode($this->_response->getBody());
    }

    public function testLoginInvalid()
    {
        $this->login($this->invalidUserCredentials);
        $this->assertResponseCode(401);
    }

    public function testLoginUnknown()
    {
        $this->login($this->unknownUserCredentials);
        $this->assertResponseCode(401);
    }

    public function testLogoutNotLoggedIn()
    {
        $this->logout();
    }

    private function logout()
    {
        $this->defaultRequestSetup();
        $this->post('/security/logout');
        $this->assertResponseOk();

        $this->session(['Auth' => []]);
    }

    public function testGetCsrf()
    {
        $this->defaultRequestSetup();
        $this->get('security/csrf');

        $this->assertResponseOk();
        $this->assertResponseContains('csrfToken');
    }

    public function testGetWithoutLogin()
    {
        $this->defaultRequestSetup();
        $this->get('/asset');
        $this->assertResponseCode(302);
    }

    public function testGetCurrentUserHasRoles()
    {
        $this->login($this->validUserCredentials);
        $this->assertResponseOk();

        $this->defaultRequestSetup();
        $this->get('/security/user');
        $this->assertResponseOk();
        $response = $this->getResponseBody();
        $this->assertNotNull($response->user->roles);
    }

    public function testGetAfterLogin()
    {
        $this->login($this->validUserCredentials);
        $this->assertResponseOk();

        $this->defaultRequestSetup();
        $this->get('/security/user');
        $this->assertResponseOk();
    }

    public function testGetAfterLogout()
    {
        $this->login($this->validUserCredentials);
        $this->assertResponseOk();
        $this->logout();

        $this->defaultRequestSetup();
        $this->get('/asset');
        $this->assertResponseCode(302);
    }

    public function testGetAfterUnsuccessfulLogin()
    {
        $this->login($this->invalidUserCredentials);
        $this->assertResponseCode(401);

        $this->defaultRequestSetup();
        $this->get('/asset');
        $this->assertResponseCode(302);
    }
}
