<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MemberInstrumentController;
use App\Test\TestCase\Controller\RestTestCase;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\MemberInstrumentServiceComponent;

class MemberInstrumentControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->InstrumentService = new MemberInstrumentServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->InstrumentService);

        parent::tearDown();
    }

    public function testIndex() {
        $responseData = $this->get('/member/1/member_instrument');

        $this->assertResponseOk();
        $this->assertResponseContains('instruments');
        $this->assertCount(2, $responseData->instruments);
    }

    public function testAdd() {
        $newInstrument = ['instrumentName' => 'Timpani'];

        $responseData = $this->post('member/1/member_instrument', $newInstrument);

        $this->assertResponseOk();
        $this->assertCount(3, $responseData->instruments);

        // Make sure it was actually created.
        $this->InstrumentService->findAll($responseData->instruments[0]->id);
    }

    public function testAddExists() {
        $newInstrument = ['instrumentName' => 'Clarinet'];

        $responseData = $this->post('member/1/member_instrument', $newInstrument);

        $this->assertResponseOk();
        $this->assertCount(2, $responseData->instruments);
    }

    public function testDelete() {
        $deletedInstrument = ['instrumentName' => 'Clarinet'];
        $this->post('/member/1/member_instrument/delete', $deletedInstrument);

        $this->assertResponseOk();
        $this->assertCount(1, $this->InstrumentService->findAll(1));
    }

    public function testDeleteNoInstrument() {
        $deletedInstrument = ['instrumentName' => 'Some other instrument'];
        $precount = $this->InstrumentService->findAll(1)->count();

        $this->post('/member/1/member_instrument/delete', $deletedInstrument);

        $this->assertResponseCode(200);
        $this->assertCount($precount, $this->InstrumentService->findAll(1));
    }

    public function testDeleteNoMember() {
        $deletedInstrument = ['instrumentName' => 'Clarinet'];
        $this->post('/member/5/member_instrument/delete', $deletedInstrument);
        $this->assertResponseCode(404);
    }
}
