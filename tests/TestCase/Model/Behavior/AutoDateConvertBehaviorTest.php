<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\AutoDateConvertBehavior;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\Event\Event;
use \ArrayObject;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class AutoDateConvertBehaviorTest extends TestCase
{
    public $AutoDateConvert;

    public function setUp()
    {
        parent::setUp();
        $this->AutoDateConvert = new AutoDateConvertBehavior(new Table());
        $this->AutoDateConvert->initialize([
            'dateTimeFields' => ['testField']
        ]);
    }

    public function tearDown()
    {
        unset($this->AutoDateConvert);

        parent::tearDown();
    }

    public function testTimeStampConversion()
    {
        $data = ['testField' => '2017-05-01T18:03Z'];

        $data = $this->executeBeforeMarshal($data);

        $this->assertInstanceOf(Time::class, $data['testField']);
    }

    public function testInvalidTimeStampLeftAsString()
    {
        $data = ['testField' => '2017-13-01T18:03Z'];

        $data = $this->executeBeforeMarshal($data);

        $this->assertInternalType('string', $data['testField']);
    }

    private function executeBeforeMarshal($data)
    {
        $event = new Event('MockEvent');
        $options = new ArrayObject();
        $data = new ArrayObject($data);

        $this->AutoDateConvert->beforeMarshal($event, $data, $options);

        return $data;
    }

    public function testValidatorRuleReturnsTrueForTime()
    {
        $validator = $this->executeBuildValidator();
        $rule = $validator->field('testField')->rule('custom');

        $isValid = $rule->process(new Time(), [], []);

        $this->assertTrue($isValid);
    }

    public function testValidatorRuleReturnsFalseForString()
    {
        $validator = $this->executeBuildValidator();
        $rule = $validator->field('testField')->rule('custom');

        $isValid = $rule->process('invalid', [], []);

        $this->assertInternalType('string', $isValid);
    }

    public function testValidatorRuleReturnsFalseForObject()
    {
        $validator = $this->executeBuildValidator();
        $rule = $validator->field('testField')->rule('custom');

        $isValid = $rule->process(new ArrayObject(), [], []);

        $this->assertInternalType('string', $isValid);
    }

    private function executeBuildValidator()
    {
        $event = new Event('MockEvent');
        $validator = new Validator();
        $options = new ArrayObject();

        $this->AutoDateConvert->buildValidator($event, $validator, $options);

        return $validator;
    }
}
