<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ScoreTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ScoreTable Test Case
 */
class ScoreTableTest extends TestCase
{

    public $Score;

    public $fixtures = [
        'app.Score',
        // Uncomment when implemented
        //'app.Performance',
        //'app.PerformanceScores'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Score') ? [] : ['className' => 'App\Model\Table\ScoreTable'];
        $this->Score = TableRegistry::get('Score', $config);
    }

    public function tearDown()
    {
        unset($this->Score);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $score = $this->Score->get(1);

        $this->assertNotNull($score);
        $this->assertEquals(1, $score->id);
    }

    public function testValidationDefault()
    {
        $score = $this->Score->get(1);
        $score = $this->Score->patchEntity($score, ['title' => '']);

        $this->assertNotEmpty($score->getErrors()['title']);
    }

    public function testDateConversionValid()
    {
        $score = $this->Score->get(1);
        $score = $this->Score->patchEntity($score, ['datePurchased' => '2015-12-01T12:00Z']);

        $this->assertInstanceOf('Cake\I18n\Time', $score['datePurchased']);
    }

    public function testDateConversionInValid()
    {
        $score = $this->Score->get(1);
        $data = ['datePurchased' => '2015-13-01T12:00Z'];

        $this->Score->patchEntity($score, $data);

        $this->assertNotEmpty($score->getErrors()['datePurchased']);
    }
}
