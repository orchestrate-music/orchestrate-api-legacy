<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RoleTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class RoleTableTest extends TestCase
{
    public $Role;

    public $fixtures = [
        'app.Role'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Role') ? [] : ['className' => 'App\Model\Table\RoleTable'];
        $this->Role = TableRegistry::get('Role', $config);
    }

    public function tearDown()
    {
        unset($this->Role);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $role = $this->Role->get(1);
        $this->assertNotNull($role);
        $this->assertEquals(1, $role->id);
    }

    public function testValidationDefault()
    {
        $role = $this->Role->get(1);
        $role = $this->Role->patchEntity($role, ['role' => null]);
        $this->assertNotEmpty($role->getErrors()['role']);
    }
}
