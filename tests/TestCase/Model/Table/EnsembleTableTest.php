<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EnsembleTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EnsembleTable Test Case
 */
class EnsembleTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Ensemble'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ensemble') ? [] : ['className' => 'App\Model\Table\EnsembleTable'];
        $this->Ensemble = TableRegistry::get('Ensemble', $config);
    }

    public function tearDown()
    {
        unset($this->Ensemble);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $ensemble = $this->Ensemble->get(1);

        $this->assertNotNull($ensemble);
        $this->assertEquals(1, $ensemble->id);
    }
}
