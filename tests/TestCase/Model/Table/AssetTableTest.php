<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AssetTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class AssetTableTest extends TestCase
{
    public $fixtures = [
        'app.Asset'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Asset') ? [] : ['className' => 'App\Model\Table\AssetTable'];
        $this->Asset = TableRegistry::get('Asset', $config);
    }

    public function tearDown()
    {
        unset($this->Asset);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $asset = $this->Asset->get(1);

        $this->assertNotNull($asset);
        $this->assertEquals(1, $asset->id);
    }

    public function testValidationDefault()
    {
        $asset = $this->Asset->get(1);
        $asset = $this->Asset->patchEntity($asset, ['description' => '']);

        $this->assertNotEmpty($asset->getErrors()['description']);
    }

    public function testDateAcquiredConversionValid()
    {
        $asset = $this->Asset->get(1);
        $data = ['dateAcquired' => '2015-12-01T12:00Z'];

        $asset = $this->Asset->patchEntity($asset, $data);

        $this->assertInstanceOf('DateTime', $asset->dateAcquired);
    }

    public function testDateAcquiredConversionInValid()
    {
        $asset = $this->Asset->get(1);
        $data = ['dateAcquired' => '2015-13-01T12:00Z'];

        $this->Asset->patchEntity($asset, $data);

        $this->assertNotEmpty($asset->getErrors()['dateAcquired']);
    }

    public function testDateDiscardedConversionValid()
    {
        $asset = $this->Asset->get(1);
        $data = ['dateDiscarded' => '2015-12-01T12:00Z'];

        $asset = $this->Asset->patchEntity($asset, $data);

        $this->assertInstanceOf('DateTime', $asset->dateDiscarded);
    }

    public function testDateDiscardedConversionInValid()
    {
        $asset = $this->Asset->get(1);
        $data = ['dateDiscarded' => '2015-13-01T12:00Z'];

        $this->Asset->patchEntity($asset, $data);

        $this->assertNotEmpty($asset->getErrors()['dateDiscarded']);
    }

    public function testInvalidNaturalNumber()
    {
        $asset = $this->Asset->get(1);
        $data = ['quantity' => -1];

        $asset = $this->Asset->patchEntity($asset, $data);

        $this->assertNotEmpty($asset->getErrors()['quantity']);
    }

    public function testInvalidNaturalNumberZero()
    {
        $asset = $this->Asset->get(1);
        $data = ['quantity' => 0];

        $asset = $this->Asset->patchEntity($asset, $data);

        $this->assertNotEmpty($asset->getErrors()['quantity']);
    }

    public function testValidNaturalNumber()
    {
        $asset = $this->Asset->get(1);
        $data = ['quantity' => 5];

        $asset = $this->Asset->patchEntity($asset, $data);

        $this->assertCount(0, $asset->getErrors());
    }
}
