<?php
namespace App\Test\TestCase;

use PHPUnit\Framework\Assert;

class CustomAssert {
    public static function entityIdHasSpecifiedPerformanceCount($entities, $entityId, $numPerformances) {
        $foundEntity = null;

        foreach ($entities as $entity) {
            if ($entity->id == $entityId) {
                $foundEntity = $entity;
                break;
            }
        }

        if (!$foundEntity) {
            Assert::fail('Specified entity (id ' + $entityId + ') not found');
        }

        Assert::assertNotNull($foundEntity->performanceCount);
        Assert::assertEquals($numPerformances, $foundEntity->performanceCount);
    }

    public static function memberHasStandardSummary($member) {
        if (method_exists($member, 'toArray')) {
            $arrayMember = $member->toArray();
        } else {
            $arrayMember = (array) $member;
        }

        self::memberHasNoPrivateFields($arrayMember);
        self::memberHasNames($arrayMember);
        self::memberHasEmbeddedInstrumentAndEnsemble($arrayMember);
    }

    public static function memberHasNoPrivateFields($member) {
        Assert::assertArrayNotHasKey('phoneNo', $member);
        Assert::assertArrayNotHasKey('address', $member);
    }

    public static function memberHasNames($member) {
        Assert::assertArrayHasKey('firstName', $member);
        Assert::assertArrayHasKey('lastName', $member);
    }

    public static function memberHasEmbeddedInstrumentAndEnsemble($member) {
        Assert::assertArrayHasKey('instruments', $member);
        Assert::assertArrayHasKey('ensemble', $member);
    }
}