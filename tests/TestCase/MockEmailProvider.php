<?php

namespace App\Test\TestCase;

use App\Controller\Component\EmailServiceComponent;
use App\Controller\Component\Email\EmailProvider;
use Cake\Core\Configure;

class MockEmailProvider implements EmailProvider
{
    private $lastSend;

    public function __construct(EmailServiceComponent $emailService)
    {
        Configure::delete('App.toEmailOverride');
        $emailService->setOverrideProvider($this);
    }

    public function __destruct()
    {
        Configure::write('App.toEmailOverride', '');
    }

    public function send($emailData)
    {
        $this->lastSend = $emailData;
    }

    public function getLastSend()
    {
        return $this->lastSend;
    }
}
