<?php
namespace App\Controller;

use App\Controller\AppController;

class ScoreController extends AppController {
    public $components = [
        'ScoreService'
    ];

    public function index() {
        $scores = $this->ScoreService->findAll();

        $this->set('score', $scores);
        $this->set('_serialize', ['score']);
    }

    public function view($id) {
        $score = $this->ScoreService->findById($id);

        $this->set('score', $score);
        $this->set('_serialize', ['score']);
    }

    public function add() {
        $this->request->allowMethod(['post']);

        $score = $this->ScoreService->create($this->request->getData());

        $this->set('score', $score);
        $this->set('_serialize', ['score']);
    }

    public function edit($id) {
        $this->request->allowMethod(['put']);

        $score = $this->ScoreService->update($id, $this->request->getData());

        $this->set('score', $score);
        $this->set('_serialize', ['score']);
    }

    public function delete($id) {
        $this->request->allowMethod(['delete']);

        $this->ScoreService->delete($id);
    }
}
