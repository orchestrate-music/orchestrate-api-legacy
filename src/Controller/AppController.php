<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $components = [
        'AuthorisationService'
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        /*
         * Enable the authentication component.
         * Tell the auth component which controller method is used for
         * logging in.
         */
        $this->loadComponent('Auth', [
            // Enable controller authorisation
            'authorize' => 'Controller',
            // Set the login function to be called to the security controller
            'loginAction' => [
                'controller' => 'Security',
                'action' => 'login'
            ]
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        /**
         * Forces the response to be in JSON
         */
        if (
            !array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->getType(), ['application/json'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        /* To always allow a certain type of request add it here to the
         * array of method names. We want everything secure, so nothing enabled
         *
         * e.g. Below, 'index' i.e. /asset
         */
        //$this->Auth->allow(['index']);
    }

    public function isAuthorized($user)
    {
        return $this->AuthorisationService->requestAndUserMatchesStandardAuthorisation($this->request, $user);
    }

    protected function serialisePropertyAndData($propertyName, $data)
    {
        $this->set($propertyName, $data);
        $this->set('_serialize', [$propertyName]);
    }
}
