<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\BadRequestException;

/**
 * MemberInstrument Controller
 *
 * @property \App\Model\Table\MemberInstrumentTable $MemberInstrument
 */
class MemberInstrumentController extends AppController {
    public $components = [
        'MemberInstrumentService'
    ];

    public function isAuthorized($user) {
        return $this->AuthorisationService->userHasRole($user, 'admin') ||
            $this->AuthorisationService->userHasRole($user, 'editor');
    }

    public function index() {
        $memberId = $this->request->getParam('member_id');
        $instruments = $this->MemberInstrumentService->findAll($memberId);

        $this->set('instruments', $instruments);
        $this->set('_serialize', ['instruments']);
    }

    public function add() {
        $this->request->allowMethod(['post']);

        $memberId = $this->request->getParam('member_id');
        $instrument = $this->request->getData()['instrumentName'];

        if (!$instrument) {
            throw new BadRequestException("Instrument must be passed as instrumentName");
        }

        $instruments = $this->MemberInstrumentService->create($memberId, $instrument);

        $this->set('instruments', $instruments);
        $this->set('_serialize', ['instruments']);
    }

    public function delete() {
        $memberId = $this->request->getParam('member_id');
        $instrument = $this->request->getData()['instrumentName'];

        if (!$instrument) {
            throw new BadRequestException("Instrument must be passed as instrumentName");
        }

        $instruments = $this->MemberInstrumentService->delete($memberId, $instrument);

        $this->set('instruments', $instruments);
        $this->set('_serialize', ['instruments']);
    }
}
