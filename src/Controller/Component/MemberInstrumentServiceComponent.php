<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\BadRequestException;

/**
 * MemberInstrumentService component
 */
class MemberInstrumentServiceComponent extends Component {
    protected $_defaultConfig = [];

    private $memberInstrumentTable;
    private $memberTable;

    public function initialize(array $config) {
        parent::initialize($config);

        $this->memberInstrumentTable = TableRegistry::get('MemberInstrument');
        $this->memberTable = TableRegistry::get('Member');
    }

    public function findAll($memberId) {
        return $this->memberInstrumentTable->find('all')
            ->where(['MemberInstrument.memberId' => $memberId]);
    }

    public function findByMemberAndInstrument($memberId, $instrumentName) {
        return $this->findAll($memberId)
            ->where(['MemberInstrument.instrumentName' => $instrumentName])
            ->first();
    }

    public function setMembersInstrumentsFromMemberMap($memberId, $member) {
        $this->removeMembersInstruments($memberId);

        if (isset($member['instruments']) && is_array($member['instruments'])) {
            $this->addInstrumentsToMember($memberId, $member['instruments']);
        }
    }

    public function addInstrumentsToMember($memberId, array $instruments) {
        foreach ($instruments as $instrument) {
            $this->create($memberId, $instrument);
        }
    }

    public function create($memberId, $instrumentName)
    {
        if (!$this->findByMemberAndInstrument($memberId, $instrumentName)) {
            $memberInstrument = $this->memberInstrumentTable->newEntity();
            $memberInstrument->memberId = $memberId;
            $memberInstrument->instrumentName = $instrumentName;

            if (!$this->memberInstrumentTable->save($memberInstrument)) {
                throw new InternalErrorException("Unable to save.");
            }
        }

        return $this->findAll($memberId);
    }

    public function removeMembersInstruments($memberId) {
        $this->memberInstrumentTable->deleteAll([
            'memberId' => $memberId
        ]);
    }

    public function delete($memberId, $instrumentName) {
        // Check member actually exists
        $this->memberTable->get($memberId);

        $currentInstrument = $this->findByMemberAndInstrument($memberId, $instrumentName);

        if ($currentInstrument) {
            if (!$this->memberInstrumentTable->delete($currentInstrument)) {
                throw new InternalErrorException('Couldn\'t delete instrument.');
            }
        }

        return $this->findAll($memberId);
    }
}
