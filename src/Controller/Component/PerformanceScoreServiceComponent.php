<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\InternalErrorException;

class PerformanceScoreServiceComponent extends Component {
    public $components = [
        'PerformanceService',
        'ScoreService'
    ];

    private $performanceScoreTable;

        public function initialize(array $config) {
        parent::initialize($config);
        $this->performanceScoreTable = TableRegistry::get('PerformanceScore');
    }

    public function findByPerformanceId($performanceId) {
        $this->PerformanceService->findById($performanceId);

        return $this->performanceScoreTable->find()
            ->contain(['Score'])
            ->where(['performanceId' => $performanceId])
            ->formatResults(function($results) {
                return $results->map(function($row) {
                    return $row->score;
                });
            });
    }

    public function addPerformanceScore($performanceId, $scoreId) {
        if (!$this->performanceScoreExists($performanceId, $scoreId)) {
            $performanceScore = $this->performanceScoreTable->newEntity();
            $performanceScore->performanceId = $performanceId;
            $performanceScore->scoreId = $scoreId;

            if (!$this->performanceScoreTable->save($performanceScore)) {
                throw new InternalErrorException('Unable to save performance score');
            }
        }
    }

    private function performanceScoreExists($performanceId, $scoreId) {
        return $this->getPerformanceScore($performanceId, $scoreId) != null;
    }

    private function getPerformanceScore($performanceId, $scoreId) {
        $this->PerformanceService->findById($performanceId);
        $this->ScoreService->findById($scoreId);

        return $this->performanceScoreTable->find()
            ->where([
                'performanceId' => $performanceId,
                'scoreId' => $scoreId
            ])
            ->first();
    }

    public function deletePerformanceScore($performanceId, $scoreId) {
        $performanceScore = $this->getPerformanceScore($performanceId, $scoreId);

        if ($performanceScore) {
            if (!$this->performanceScoreTable->delete($performanceScore)) {
                throw new InternalErrorException('Unable to delete performance score');
            }
        }
    }
}
