<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\InternalErrorException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;

class PerformanceServiceComponent extends Component {
    public $components = [
        'ConcertService',
        'UtilityService'
    ];

    private $performanceTable;

    public function initialize(array $config) {
        parent::initialize($config);
        $this->performanceTable = TableRegistry::get('Performance');
    }

    public function findAll() {
        return $this->performanceTable->find();
    }

    public function findByConcertId($concertId) {
        $this->ConcertService->findById($concertId);

        return $this->performanceTable->find()
            ->contain(['Ensemble'])
            ->where(['concertId' => $concertId]);
    }

    public function findById($performanceId) {
        return $this->performanceTable->get($performanceId, [
            'contain' => [
                'Ensemble',
                'Concert',
                'Score',
                'Member' => function($q) {
                    return $q->select(['id', 'firstName', 'lastName']);
                }
            ]
        ]);
    }

    public function create($performanceMap) {
        // $performanceMap can also contain a list of members and scores for this performance
        $performanceMap = $this->preparePerformanceMap($performanceMap);
        $performance = $this->performanceTable->newEntity($performanceMap);
        $performance = $this->savePerformance($performance);

        return $this->findById($performance->id);
    }

    private function preparePerformanceMap($performanceMap) {
        $fieldNameMap = [
            'members' => 'member',
            'scores' => 'score'
        ];

        return $this->UtilityService->convertMapToIdUpdateStructure($performanceMap, $fieldNameMap);
    }

    private function savePerformance($performance) {
        if ($performance->getErrors()) {
            throw new BadRequestException('Invalid Performance');
        }

        try {
            $saved = $this->performanceTable->save($performance);
        } catch (\PDOException $e) {
            throw new BadRequestException('Unable to save performance - check target concert' .
                'and ensemble');
        }

        if (!$saved) {
            throw new InternalErrorException('Unable to save performance');
        }

        return $performance;
    }

    public function update($performanceId, $performanceMap) {
        $performance = $this->findById($performanceId);
        $performanceMap = $this->preparePerformanceMap($performanceMap);

        if ($performance) {
            $performance = $this->performanceTable->patchEntity($performance, $performanceMap);
            $performance = $this->savePerformance($performance);
        } else {
            throw new RecordNotFoundException('Unable to find performance');
        }

        return $this->findById($performance->id);
    }

    public function delete($performanceId) {
        $performance = $this->findById($performanceId);

        if ($performance) {
            if (!$this->performanceTable->delete($performance)) {
                throw new InternalErrorException('Unable to delete performance');
            }
        } else {
            throw new RecordNotFoundException('Unable to find performance');
        }
    }
}
