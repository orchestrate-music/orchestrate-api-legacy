<?php
namespace App\Controller\Component\Email;

interface EmailProvider {
    function send($emailData);
}