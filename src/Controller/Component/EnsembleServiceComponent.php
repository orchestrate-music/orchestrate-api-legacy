<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\I18n\Time;

class EnsembleServiceComponent extends Component
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        /* Get the Table DAO/Table Object */
        $this->ensembleTable = TableRegistry::get('Ensemble');
    }

    public function findAll()
    {
        $query = $this->ensembleTable->find();

        $query->select([
            'performanceCount' => $query->func()->count('Concert.id')
        ])
            ->contain(['Member' => function ($q) {
                // We disable auto fields so that only the fields
                // we want are returned
                return $q->select(['id', 'firstName', 'lastName'])
                    ->where(['OR' => [
                        'EnsembleMembership.dateLeft IS NULL',
                        'EnsembleMembership.dateLeft >' => new Time(),
                    ]])
                    ->enableAutoFields(false);
            }])
            ->leftJoinWith('Concert')
            ->group('Ensemble.id')
            ->enableAutoFields(true);

        return $query;
    }

    public function findById($id = null)
    {
        return $this->ensembleTable->get($id, [
            'contain' => ['Member' => function ($q) {
                return $q->select(['id', 'firstName', 'lastName'])
                    ->where(['OR' => [
                        'EnsembleMembership.dateLeft IS NULL',
                        'EnsembleMembership.dateLeft >' => new Time(),
                    ]])
                    ->enableAutoFields(false);
            }]
        ]);
    }

    public function create($ensembleMap = null)
    {
        // Attempt to map the map to an ensemble
        $ensemble = $this->ensembleTable->newEntity($ensembleMap);
        $this->saveEnsemble($ensemble);

        return $ensemble;
    }

    private function saveEnsemble($ensemble)
    {
        if ($ensemble->getErrors()) {
            throw new BadRequestException($ensemble->getErrors());
        }

        if (!$this->ensembleTable->save($ensemble)) {
            throw new InternalErrorException("Invalid ensemble.");
        }
    }

    public function update($id = null, $ensembleMap = null)
    {
        // Attempt to map the map to an ensemble
        $ensemble = $this->findById($id);

        $ensemble = $this->ensembleTable->patchEntity($ensemble, $ensembleMap);
        $this->saveEnsemble($ensemble);

        return $ensemble;
    }

    public function delete($id = null)
    {
        $ensemble = $this->findById($id);

        if (!$this->ensembleTable->delete($ensemble)) {
            throw new InternalErrorException('Couldn\'t delete ensemble.');
        }
    }

    public function count()
    {
        return $this->ensembleTable->find()->count();
    }
}
