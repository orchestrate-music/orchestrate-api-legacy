<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\I18n\Time;

class AssetLoanServiceComponent extends Component {
    public $components = [
        'AssetService',
        'MemberService'
    ];

    private $assetLoanTable;

    public function initialize(array $config) {
        parent::initialize($config);
        $this->assetLoanTable = TableRegistry::get('AssetLoan');
    }

    public function findById($loanId) {
        return $this->assetLoanTable->get($loanId, [
            'contain' => [
                'Asset',
                'Member' => function($q) {
                    return $q->select(['id', 'firstName', 'lastName']);
                }
            ]
        ]);
    }

    public function update($id, $loanMap) {
        $loan = $this->findById($id);

        if (isset($loanMap['dateBorrowed'])) {
            $loan->dateBorrowed = $loanMap['dateBorrowed'];
        }
        if (isset($loanMap['dateReturned'])) {
            $loan->dateReturned = $loanMap['dateReturned'];
        }

        $this->saveLoan($loan);

        return $this->findById($id);
    }

    public function delete($id) {
        $loan = $this->findById($id);

        if (!$this->assetLoanTable->delete($loan)) {
            throw new InternalErrorException('Failed to delete loan');
        }
    }

    public function getHistoryByAssetId($assetId) {
        return $this->assetLoanTable->find()
            ->contain(['Member' => function ($q) {
                return $q->select(['id', 'firstName', 'lastName']);
            }])
            ->where(['assetId' => $assetId])
            ->order(['dateBorrowed' => 'DESC']);
    }

    public function getCurrentLoanForAsset($assetId) {
        $loan = $this->getCurrentLoan($assetId);

        if (!$loan) {
            throw new RecordNotFoundException('No Loan for asset');
        }

        return $loan;
    }

    private function getCurrentLoan($assetId) {
        return $this->getHistoryByAssetId($assetId)
            ->where(['dateReturned IS NULL'])
            ->first();
    }

    public function loanAssetToMember($assetId, $memberId) {
        // Verify Asset and Member exist
        $this->AssetService->findById($assetId);
        $this->MemberService->findById($memberId);

        $currLoan = $this->getCurrentLoan($assetId);

        if ($currLoan) {
            if ($currLoan->member->id != $memberId) {
                throw new BadRequestException('Asset is already on loan to member with id ' . $currLoan->member->id);
            }
        } else {
            $this->addAssetLoan($assetId, $memberId);
        }

        return $this->getCurrentLoanForAsset($assetId);
    }

    private function addAssetLoan($assetId, $memberId) {
        $loanMap = [
            'assetId' => $assetId,
            'memberId' => $memberId,
            'dateBorrowed' => new Time()
        ];

        $this->saveLoanMap($loanMap);
    }

    private function saveLoanMap($loanMap) {
        $loan = $this->assetLoanTable->newEntity($loanMap);
        $this->saveLoan($loan);
    }

    private function saveLoan($loan) {
        if ($loan->getErrors()) {
            throw new BadRequestException(print_r($loan->getErrors(), true));
        }

        if (!$this->assetLoanTable->save($loan)) {
            throw new InternalErrorException("Failed to save loan information");
        }
    }

    public function returnAssetLoan($assetId) {
        $currLoan = $this->getCurrentLoan($assetId);

        if ($currLoan) {
            $this->returnLoan($currLoan);
            $mostRecentLoan = $currLoan;
        } else {
            $mostRecentLoan = $this->getHistoryByAssetId($assetId)->first();
        }

        return $this->findById($mostRecentLoan->id);
    }

    private function returnLoan($loan) {
        $loan->dateReturned = new Time();
        $this->saveLoan($loan);
    }
}
