<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\InternalErrorException;

class PerformanceMemberServiceComponent extends Component {
    public $components = [
        'PerformanceService',
        'MemberService'
    ];

    private $performanceMemberTable;

    public function initialize(array $config) {
        parent::initialize($config);
        $this->performanceMemberTable = TableRegistry::get('PerformanceMember');
    }

    public function findByPerformanceId($performanceId) {
        $this->PerformanceService->findById($performanceId);

        return $this->performanceMemberTable->find()
            ->contain(['Member' => function($q) {
                return $q->select(['id', 'firstName', 'lastName']);
            }])
            ->where(['performanceId' => $performanceId])
            ->formatResults(function($results) {
                return $results->map(function($row) {
                    return $row->member;
                });
            });
    }

    public function addPerformanceMembership($performanceId, $memberId) {
        if (!$this->performanceMembershipExists($performanceId, $memberId)) {
            $performanceMembership = $this->performanceMemberTable->newEntity();
            $performanceMembership->performanceId = $performanceId;
            $performanceMembership->memberId = $memberId;

            if (!$this->performanceMemberTable->save($performanceMembership)) {
                throw new InternalErrorException('Unable to save performance membership');
            }
        }
    }

    private function performanceMembershipExists($performanceId, $memberId) {
        return $this->getPerformanceMembership($performanceId, $memberId) != null;
    }

    private function getPerformanceMembership($performanceId, $memberId) {
        $this->PerformanceService->findById($performanceId);
        $this->MemberService->findById($memberId);

        return $this->performanceMemberTable->find()
            ->where([
                'performanceId' => $performanceId,
                'memberId' => $memberId
            ])
            ->first();
    }

    public function deletePerformanceMembership($performanceId, $memberId) {
        $performanceMembership = $this->getPerformanceMembership($performanceId, $memberId);

        if ($performanceMembership) {
            if (!$this->performanceMemberTable->delete($performanceMembership)) {
                throw new InternalErrorException('Unable to delete performance membership');
            }
        }
    }
}
