<?php
namespace App\Controller;

use App\Controller\AppController;

class MemberSummaryController extends AppController {
    public $components = [
        'MemberService'
    ];

    public function allMembers() {
        $members = $this->MemberService->getAllMembersSummary();

        $this->set('member', $members);
        $this->set('_serialize', ['member']);
    }

    public function currentMembers($id = null) {
        $members = $this->MemberService->getCurrentMemberSummary();

        $this->set('member', $members);
        $this->set('_serialize', ['member']);
    }
}
