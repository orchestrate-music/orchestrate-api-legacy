<?php
namespace App\Controller;

use App\Controller\AppController;

class ConcertController extends AppController {
    /* Automatically include the Asset Service */
    public $components = [
      'ConcertService'
    ];

    public function index() {
        $concerts = $this->ConcertService->findAll();

        $this->set('concert', $concerts);
        $this->set('_serialize', ['concert']);
    }

    public function view($id = null) {
        $concert = $this->ConcertService->findById($id);

        $this->set('concert', $concert);
        $this->set('_serialize', ['concert']);
    }

    public function add() {
        $this->request->allowMethod(['post']);

        $concert = $this->ConcertService->create($this->request->getData());

        $this->set('concert', $concert);
        $this->set('_serialize', ['concert']);
    }

    public function edit($id = null) {
        $this->request->allowMethod(['put']);

        $concert = $this->ConcertService->update($id, $this->request->getData());

        $this->set('concert', $concert);
        $this->set('_serialize', ['concert']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['delete']);

        $this->ConcertService->delete($id);
    }

    public function byMemberId() {
        $memberId = $this->request->getParam('member_id');
        $memberConcerts = $this->ConcertService->findByMemberId($memberId);

        $this->set('concert', $memberConcerts);
        $this->set('_serialize', ['concert']);
    }
}
