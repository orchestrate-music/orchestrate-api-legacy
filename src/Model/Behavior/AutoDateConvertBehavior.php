<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\PropertyMarshalInterface;
use Cake\i18n\Time;
use Cake\Event\Event;
use \ArrayObject;
use Cake\Validation\Validator;

class AutoDateConvertBehavior extends Behavior
{
    protected $_defaultConfig = [];

    private $autoConvertFields = [];

    public function initialize(array $config)
    {
        if (isset($config['dateTimeFields'])
            && is_array($config['dateTimeFields'])) {
            foreach ($config['dateTimeFields'] as $dateTimeField) {
                if (is_string($dateTimeField)) {
                    $this->autoConvertFields[] = $dateTimeField;
                }
            }
        }
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($this->autoConvertFields as $dateTimeField) {
            if (isset($data[$dateTimeField])) {
                $originalValue = $data[$dateTimeField];

                try {
                    $convertedValue = new Time($originalValue);
                } catch (\Exception $e) {
                    $convertedValue = $originalValue;
                }

                $data[$dateTimeField] = $convertedValue;
            }
        }
    }

    public function buildValidator(Event $event, Validator $validator, $name)
    {
        foreach ($this->autoConvertFields as $dateTimeField) {
            $validator->add($dateTimeField, 'custom', [
                'rule' => function ($value, $context) {
                    return $value instanceof Time;
                },
                'message' => 'Specified DateTime is invalid'
            ]);
        }
    }
}
