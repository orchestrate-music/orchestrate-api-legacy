<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Score Entity
 *
 * @property int $id
 * @property string $title
 * @property string $composer
 * @property string $arranger
 * @property string $genre
 * @property int $grade
 * @property \Cake\I18n\Time $duration
 * @property \Cake\I18n\Time $datePurchased
 * @property float $valuePaid
 * @property string $location
 * @property string $notes
 *
 * @property \App\Model\Entity\Performance[] $performance
 */
class Score extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
