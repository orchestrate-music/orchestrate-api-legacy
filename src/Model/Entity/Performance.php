<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Performance Entity
 *
 * @property int $id
 * @property int $ensembleId
 * @property int $concertId
 * @property bool $countsAsSeperate
 *
 * @property \App\Model\Entity\Ensemble $ensemble
 * @property \App\Model\Entity\Concert $concert
 * @property \App\Model\Entity\Member[] $member
 * @property \App\Model\Entity\Score[] $score
 */
class Performance extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
