<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Performance Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Member
 *
 * @method \App\Model\Entity\Performance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Performance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Performance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Performance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Performance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Performance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Performance findOrCreate($search, callable $callback = null)
 */
class PerformanceTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('performance');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Ensemble', [
            'foreignKey' => 'ensembleId'
        ]);
        $this->belongsTo('Concert', [
            'foreignKey' => 'concertId'
        ]);

        $this->belongsToMany('Member', [
            'foreignKey' => 'performanceId',
            'targetForeignKey' => 'memberId',
            'joinTable' => 'performance_member'
        ]);
        $this->belongsToMany('Score', [
            'foreignKey' => 'performanceId',
            'targetForeignKey' => 'scoreId',
            'joinTable' => 'performance_score'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ensembleId')
            ->requirePresence('ensembleId', 'create')
            ->notEmpty('ensembleId');

        $validator
            ->integer('concertId')
            ->requirePresence('concertId', 'create')
            ->notEmpty('concertId');

        $validator
            ->boolean('countsAsSeperate')
            ->requirePresence('countsAsSeperate', 'create')
            ->notEmpty('countsAsSeperate');

        return $validator;
    }
}
