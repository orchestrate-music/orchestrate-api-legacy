<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ensemble Model
 *
 * @method \App\Model\Entity\Ensemble get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ensemble newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ensemble[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ensemble|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ensemble patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ensemble[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ensemble findOrCreate($search, callable $callback = null, $options = [])
 */
class EnsembleTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ensemble');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Member', [
            'foreignKey' => 'ensembleId',
            'targetForeignKey' => 'memberId',
            'joinTable' => 'ensemble_membership'
        ]);
        $this->belongsToMany('Concert', [
            'foreignKey' => 'ensembleId',
            'targetForeignKey' => 'concertId',
            'joinTable' => 'performance'
        ]);
        $this->belongsToMany('Role', [
            'foreignKey' => 'ensembleId',
            'targetForeignKey' => 'roleId',
            'joinTable' => 'ensemble_role',
            'propertyName' => 'roles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('isHidden')
            ->requirePresence('isHidden', 'create')
            ->notEmpty('isHidden');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }
}
