<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use \ArrayObject;
use Cake\Event\Event;

/**
 * Concert Model
 *
 * @method \App\Model\Entity\Concert get($primaryKey, $options = [])
 * @method \App\Model\Entity\Concert newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Concert[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Concert|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Concert patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Concert[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Concert findOrCreate($search, callable $callback = null)
 */
class ConcertTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('AutoDateConvert', [
            'dateTimeFields' => ['date']
        ]);

        $this->setTable('concert');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Performance', [
            'foreignKey' => 'concertId'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('occasion');

        $validator
            ->allowEmpty('location');

        $validator
            ->dateTime('date')
            ->allowEmpty('date');

        $validator
            ->allowEmpty('notes');

        return $validator;
    }
}
